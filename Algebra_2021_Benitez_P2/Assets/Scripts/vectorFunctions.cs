﻿using EjerciciosAlgebra;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomMath;

public class vectorFunctions : MonoBehaviour
{
    public enum Ejercicios { uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve, diez };
    public Ejercicios ejercicio;
    public Color color = Color.yellow;

    public Vector3 vecA;
    public Vector3 vecB;

    private Vec3 vectorA;
    private Vec3 vectorB;
    Vec3 vectorC;
    float t = 0;

    void Start()
    {
        vectorA = new Vec3(vecA);
        vectorB = new Vec3(vecB);

        VectorDebugger.EnableCoordinates();
        VectorDebugger.EnableEditorView();

        VectorDebugger.AddVector(new Vector3(vectorA.x, vectorA.y, vectorA.z), "vectorA");
        VectorDebugger.UpdateColor("vectorA", Color.white);

        VectorDebugger.AddVector(new Vector3(vectorB.x, vectorB.y, vectorB.z), "vectorB");
        VectorDebugger.UpdateColor("vectorB", Color.black);

        VectorDebugger.AddVector(new Vector3(vectorC.x, vectorC.y, vectorC.z), "vectorC");
        VectorDebugger.UpdateColor("vectorC", Color.yellow);
    }

    void Update()
    {
        VectorDebugger.UpdatePosition("vectorA", new Vector3(vectorA.x, vectorA.y, vectorA.z));
        VectorDebugger.UpdatePosition("vectorB", new Vector3(vectorB.x, vectorB.y, vectorB.z));
        VectorDebugger.UpdatePosition("vectorC", new Vector3(vectorC.x, vectorC.y, vectorC.z));

        switch (ejercicio)
        {
            case Ejercicios.uno:   // Suma
                vectorC = vectorA + vectorB;
                break;
            case Ejercicios.dos:   // Resta
                vectorC = vectorB - vectorA;
                break;
            case Ejercicios.tres:   // Escalar
                vectorC = Vec3.Scale(vectorA, vectorB);
                break;
            case Ejercicios.cuatro:   // Cruz
                vectorC = Vec3.Cross(vectorB, vectorA);
                break;
            case Ejercicios.cinco:   // Lerp
                t += 1 * Time.deltaTime;
                if (t > 1) t = 0;
                vectorC = Vec3.Lerp(vectorA, vectorB,t);
                break;
            case Ejercicios.seis:   // Max
                vectorC = Vec3.Max(vectorA, vectorB);
                break;
            case Ejercicios.siete:   // Proyeccion
                vectorC = Vec3.Project(vectorA, vectorB);
                break;
            case Ejercicios.ocho:   // Vector normalizado de la distancia entre el vectorA y el vectorB
                vectorC = vectorA + vectorB;
                vectorC = Vec3.Distance(vectorA, vectorB) * Vec3.Normalize(vectorC);
                break;
            case Ejercicios.nueve:   // Reflexion del vectorA sobre la normal del vectorB
                vectorC = Vec3.Reflect(vectorA, Vec3.Normalize(vectorB));
                break;
            case Ejercicios.diez:   // Lerp Unclamped
                t += 1 * Time.deltaTime;
                vectorC = Vec3.LerpUnclamped(vectorB, vectorA, t);
                break;
            default:
                break;
        }
    }
}